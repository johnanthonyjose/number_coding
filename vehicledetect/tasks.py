from number_coding.celery_app import app

@app.task()
def detectvehicles(videoname, opts):
    from number_coding.vehicledetect.detectvehicles2 import VehicleTracker

    track = VehicleTracker(opts)
    track.select_camera(videoname)
    track.start()

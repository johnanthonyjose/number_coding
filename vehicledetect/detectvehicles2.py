import os, sys
#try:
#    from darkflow.net.build import TFNet
#except ImportError:
sys.path.append('/home/aicatch/PycharmProjects/number_coding/number_coding/vehicledetect')
from darkflow.net.build import TFNet

import cv2
import time
import logging
from number_coding.platedetect.getplatearea2 import GetPlateArea
from pymongo import MongoClient
fps = 30

options = {"config": './cfg',
           'binary': './bin',
           'imgdir': './sample_img/',
           "model": "./cfg/yolo.cfg",
           "load": "./bin/yolo.weights",
           "threshold": 0.1,
           "gpu": 0.7}

class VehicleTracker():

    trackertype="KCF"
    def __init__(self, opts):
        super().__init__()
        try:
            self.reinitialize = True
            self.framecount = 0
            self.tfnet = TFNet(opts['yolo_opts'])
            self.boxes = []
            #self.platearea = ActorSystem().createActor(GetPlateArea)
            self.fileprefix = 'vehicle'
            self.fileext = '.png'

            self.client = MongoClient('mongodb://localhost:27017')
            self.collection = self.client.live

            self.plater = GetPlateArea(opts)

        #def receiveMessage(self, message, sender):
        #logging.info('VehicleTracker actor got: %s', message)

            #self.select_camera("/home/aicatch/front-end-rails/public/MASTER/VIDEOS/ptuazon_underpass/2018/01/11/14/video20180111_140001.avi")
            #self.select_camera(message['name'])
            #job = q.enqueue_call(func=execute_docker_static, args=(defacto,), timeout=600)
            #self.start()
            #frameseq = os.path.join(self.pathdir, self.fileprefix + "*" + self.fileext)
            #ActorSystem().tell(self.platearea, dict(name=frameseq, config=message['config']))

        except Exception as e:
            print (Exception("VehicleTracker Error ", e))
        pass

    def select_camera(self, cam):
        capture = cv2.VideoCapture(cam)
        self.rawname = cam
        if cam==0:
            self.rawname="sample_vid/videowebcam.avi"
        success = capture.isOpened()
        if not success:
            print("Cannot read video...")

        self.fix_directory(cam)
        self.cap = capture
        self.reinitialize = True
        self.framecount = 0

        return success

    def fix_directory(self, cam):
        self.pathdir = cam.rsplit('.', 1)[0]

        if not os.path.exists(self.pathdir):
            os.makedirs(self.pathdir)
            print("Created a new directory ", self.pathdir)

    def pointsflow2rect(self, topleft, bottomright):
        return topleft['x'], topleft['y'], abs(bottomright['x']-topleft['x']), abs(bottomright['y']-topleft['y'])
    
    def create_trackers(self, num):
        return [cv2.Tracker_create(self.trackertype) for i in range(num)]
         
    """Detect Method
    handles the detect and reinitialization mechanism
    """
    def initialize_boxes(self, frame):
        # add a list of boxes:
        result = self.tfnet.return_predict(frame)
        self.boxes = tuple(self.pointsflow2rect(item['topleft'], item['bottomright']) for item in result if item['label'] in ['car','bus'])
        ok = False
        try:
            # TODO filter boxes 

            if len(self.boxes)==0:
                print("No cars detected...")
                return ok, None
            print("%s cars are detected.."%len(self.boxes))
            # Start creating multiple instance of tracker per box
            multitrack = self.create_trackers(len(self.boxes))

            # Initialize each tracker object
            [obj.init(frame, box) for obj, box in zip(multitrack,self.boxes)]
            ok = True
            self.reinitialize = False
        except IndexError:
            print("No Detected cars...")

        except Exception as err:
            raise Exception(err)

        return ok, multitrack

    def update(self, trackers, image):
        newboxes = [trak.update(image)[1] for trak in trackers]
        self.boxes = newboxes

    def clean_up(self, dictframes):

        # Write a document to MongoDB
        frameseq = os.path.join(self.pathdir, self.fileprefix + "*" + self.fileext)
        self.collection.vehicles.insert_one({"video_name": self.rawname,
                                             "framesequence_name": frameseq,
                                             "vehicle_frames": dictframes}
                                            )

        # Release Memories
        self.cap.release()
        cv2.destroyAllWindows()

        # Call Plate area detection This is process_vid2.py
        #print(frameseq)
        self.plater.start_dockerCTPN(frameseq, self.collection)

    """
    Iterate over the whole video stream
    """
    def start(self):
        # TODO async message passing per frame
        vehicle_count = 0
        dictFrames = {}
        while self.cap.isOpened():
            ok, image = self.cap.read()
            if not ok:
                print ('no image read')
                break

            if self.reinitialize:
                print ("Reinitializing car detection")
                detectsuccess, trackers = self.initialize_boxes(image)

            if detectsuccess:
                self.update(trackers, image)


                for newbox in self.boxes:
                    #print(newbox)
                    p1 = (int(newbox[0]), int(newbox[1])) #Top Left
                    p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3])) #Bottom Right

                    # Getting ROI
                    # NumPy style
                    # roi = im[top-left(Y):bottom-right(Y),top-left(X):bottom-right(X)]
                    imroi = image[p1[1]:p2[1],p1[0]:p2[0]]
                    filedir, fileraw = self.rawname.rsplit('/', 1)
                    raw, exten = fileraw.rsplit('.', 1)
                    filename = self.fileprefix + '%04d'%(vehicle_count)
                    newname = os.path.join(self.pathdir, filename + self.fileext)
                    cv2.imwrite(newname,imroi)
                    dictFrames[filename] = self.framecount
                    # Draw Rectangle for visualization
                    #cv2.rectangle(image, p1, p2, (200,0,0))
                    vehicle_count+=1

            # Increment frame count
            self.framecount+=1
            if self.framecount%(fps/2)==0:
            #    print ("My count is ",self.framecount)
                self.reinitialize=True

            # cv2.imshow("tracking", image)
            # k = cv2.waitKey(1) & 0xff
            # if k == 27: break  # esc pressed

        self.clean_up(dictFrames)



def unit_test(videoname, opts):
    track = VehicleTracker(opts)
    track.select_camera(videoname)
    track.start()


if __name__=='__main__':

#    file = '/home/aicatch/front-end-rails/public/MASTER/VIDEOS/ptuazon_underpass/2018/01/11/14/video20180111_140011.avi'
#    unit_test(file)
    pass

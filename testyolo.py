try:
    from number_coding.vehicledetect.tasks import detectvehicles
    from number_coding.jsonconfencoder import run_config
except ImportError:
    from vehicledetect.tasks import detectvehicles
    from jsonconfencoder import run_config

if __name__=='__main__':
    file = '/home/aicatch/front-end-rails/public/MASTER/VIDEOS/ptuazon_underpass/2018/01/11/14/video20180111_140011.avi'

    # Get configuration file
    conf = run_config()
    detectvehicles.delay(file, conf)
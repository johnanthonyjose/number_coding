from rq import Queue
from redis import Redis
from scratch import execute_ctpn
import time

# Delay execution of count_words_at_url('http://nvie.com')
job = q.enqueue(execute_ctpn, 'VIDEOS/estrada/2018/01/05/17/video20180105_171732.avi', timeout=600)
print (job.result)   # => None

# Now, wait a while, until the worker is finished
time.sleep(10)
print (job.result)  # => 889

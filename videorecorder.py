
#!/usr/bin/env python

'''
Multithreaded video processing sample.
Usage:
   video_threaded.py {<video device number>|<video file name>}

   Shows how python threading capabilities can be used
   to organize parallel captured frame processing pipeline
   for smoother playback.

Keyboard shortcuts:

   ESC - exit
   space - switch between multi and single threaded processing
'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2

from multiprocessing.pool import ThreadPool
from collections import deque

from common import clock, draw_str, StatValue
from jsonconfencoder import run_config
def process_frame(frame, t0):
    # some intensive computation...
    # roi = im[top-left(Y):bottom-right(Y),top-left(X):bottom-right(X)]
    # 800 x 448
    frame = frame[30:frame.shape[0], 0:frame.shape[1]]
    #frame = cv2.medianBlur(frame, 19)
    #frame = cv2.medianBlur(frame, 19)
    return frame, t0

if __name__ == '__main__':
    print(__doc__)
    config = run_config()
   # cam = "rtsp://root:pass@10.0.0.211/axis-media/media.amp"
    cam = config['video_input']
    cap = cv2.VideoCapture(cam)

    threadn = cv2.getNumberOfCPUs()
    pool = ThreadPool(processes = threadn)
    pending = deque()

    threaded_mode = True

    latency = StatValue()
    frame_interval = StatValue()
    last_frame_time = clock()
    while True:
        while len(pending) > 0 and pending[0].ready():
            res, t0 = pending.popleft().get()
            latency.update(clock() - t0)
            draw_str(res, (20, 20), "threaded      :  " + str(threaded_mode))
            draw_str(res, (20, 40), "latency        :  %.1f ms" % (latency.value*1000))
            draw_str(res, (20, 60), "frame interval :  %.1f ms" % (frame_interval.value*1000))
            cv2.imshow('threaded video', res)
        if len(pending) < threadn:
            ret, frame = cap.read()
            t = clock()
            frame_interval.update(t - last_frame_time)
            last_frame_time = t
            task = pool.apply_async(process_frame, (frame.copy(), t))
            pending.append(task)

        ch = cv2.waitKey(1)
        if ch == 27:
            break
cv2.destroyAllWindows()





import numpy as np
import cv2
from datetime import datetime, timedelta
import time
import os

# import the necessary packages
import threading
from thespian.actors import *
import logging
from collections import deque
# Import custom class
from number_coding.videoprocess import VideoProcess
# from getplatearea import GetPlateArea
import glob
from number_coding.jsonconfencoder import run_config
import sys
from multiprocessing.pool import ThreadPool
from celery import Celery

logging.basicConfig(
    level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
)

class VideoQueue:

    def __init__(self, opts):

        # Initialize video feed
        self.cap = cv2.VideoCapture(opts['video_input'])
        time.sleep(2.0)

        # store the maximum buffer size of frames to be kept
        # in memory along with the sleep timeout during threading
        self.bufSize = opts['video_buffersize']
        self.timeout = opts['video_timeout']

        # initialize the buffer of frames, queue of frames that
        # need to be written to file, video writer, writer thread,
        # and boolean indicating whether recording has started or not
        self.frames = deque()
        self.Q = None
        self.recordtime = timedelta(seconds=1)

        try:
            [os.remove(i) for i in glob.glob('*.log')]
            print('Removed all log files...')
        except OSError:
            print('No log files to removed...')

        # Start Actor
        self.actsys = ActorSystem(logDefs=opts['actor_log_cfg'])

        # Create video process actor
        self.videoprocess = self.actsys.createActor(VideoProcess)

        self.filename = ''
        self.opts = opts

        #start daemon thread
        threadn = cv2.getNumberOfCPUs()
        self.pool = ThreadPool(processes = 2)
        self.pending = deque()

        #retrieve_thread = threading.Thread(target=self.retrieve_video(), args=(), daemon=True)
        #retrieve_thread.start()


        
    # Update Frame Queues
    def update(self, currentframe):
        # update the frames buffer
        # Send message to VideoProcess Actor
        sending_message = {'current_frame': currentframe,
                           'sender_name': 'frame_reader',
                           'sender_cmd': 'write',
                           'name': self.filename,
                           'config': self.opts}

        self.actsys.tell(self.videoprocess, sending_message)
        return

    # Updates recorded time and existing current working directory
    def update_time(self):
        self.recordtime = datetime.now()

        print(self.recordtime)

        # Check and/or create directory if needed
        if self.opts['dirstruct'] == 'v2':
            dirpath = os.path.join(self.opts['root'], self.opts['dirpath']['videos_raw'], self.opts['camera'], self.recordtime.strftime("%Y/%m/%d/%H"))
        else:
            dirpath = os.path.join(os.getcwd(), self.recordtime.strftime("%Y/%m/%d/%H/%M"))

        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
            print("Directory %s does not exists. creating..." % (dirpath))

        # Update Filename
        self.filename = "{}/{}{}.avi".format(dirpath, self.opts['video_fileprefix'], self.recordtime.strftime("%Y%m%d_%H%M%S"))

        return

    # Start Writing
    def transfer(self):
        logging.debug('Starting')
        self.update_time()

        sending_message = {'sender_name': 'frame_reader',
                           'sender_cmd': 'process',
                           'name': self.filename,
                           'config': self.opts
                           }
        print(self.actsys.ask(self.videoprocess, sending_message))

        logging.debug('Exiting')
    def parse_seconds(self, sec):
        if sec < 0:
            raise ValueError("The input sec should be a positive number")
        elif sec < 60:
            secdelta = timedelta(seconds=sec)
        elif sec < 3600:
            minuto = sec // 60
            segundo = sec % 60
            secdelta = timedelta(seconds=segundo, minutes=minuto)
        else:
            raise ValueError("The input value %s is too big"%(sec))

        return secdelta

    def retrieve_video(self):
        logging.debug('Starting')
        self.update_time()
        
        while self.cap.isOpened():
    
            # Read Frames
            ret, frame = self.cap.read()

            # Cut upper part of frame to remove noise
            try:
                frame = frame[30:frame.shape[0], 0:frame.shape[1]]

                #Start Stacking task queue
                task = self.pool.apply_async(self.update, (frame.copy(),))
                self.pending.append(task)
                # Stack Up the Queue
                if (datetime.now() - self.recordtime) >= self.parse_seconds(self.opts['video_secondsperfile']):
                    task2 = self.pool.apply_async(self.transfer)
                    self.pending.append(task2)
                    transfer_thread = threading.Thread(target=self.transfer(), args=())
                    transfer_thread.start()

            except AttributeError:
                continue

            finally:
                cv2.imshow("video", frame)
                ch = cv2.waitKey(1)
                if ch == 27:
                    break
    
        # Release everything if job is finished
        ActorSystem().shutdown()
        self.cap.release()
        cv2.destroyAllWindows()
        logging.debug('Exiting')
        return

def checkInstallation(rv):
    currentVersion = sys.version_info
    if currentVersion[0] == rv[0] and currentVersion[1] >= rv[1]:
        pass
    else:
        sys.stderr.write( "[%s] - Error: Your Python interpreter must be %d.%d or greater (within major version %d)\n" % (sys.argv[0], rv[0], rv[1], rv[0]) )
        sys.exit(-1)
    return 0

if __name__=='__main__':
    checkInstallation((3, 4))
    # Get configuration file
    conf = run_config()

    # Start videoprocessor object
    recorder = VideoQueue(conf)
    recorder.retrieve_video()
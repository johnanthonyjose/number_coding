import json
import os
from datetime import timedelta
import logging

def run_config():
    config = dict()

    # Folder Structure
    config['root'] = os.path.join(os.environ['HOME'] , 'front-end-rails', 'public', 'MASTER')
    config['dockerroot'] = '/opt/CTPN'
 #   config['camera'] = 'demo'
    #config['camera'] = 'webcam'
    #config['camera'] = 'estrada'
    config['camera'] = 'ptuazon_underpass'

    # Either v1 or v2 only
    # v1 means:
    #   video_path,image_path,plate_path => stored in year/month/day/hour/min
    #   video_name                       => stored as video_prefix + '_' + YYYYMMDDHHmmSS + '.avi'
    #   image_name                       => stored as image_prefix + image_num + '.png'
    #   plate_name                       => stored as plate_prefix + image_num + '.png'
    # v2 means:
    #   raw_video_path                   => stored in root/videos/cam1/yyyy/mm/dd/hh/mm
    #   image_path                       => stored in root/images
    #   plate_path                       => stored in root/plates
    #   video_name                       => stored as YYYYMMDD_HHmmSS + '.avi'
    #   image_name                       => stored as YYYYMMDD_HHmmSS + '_' + image_prefix + image_num + '.png'
    #   plate_name                       => stored as YYYYMMDD_HHmmSS + '_' + image_prefix + image_num + '.png'
    config['dirstruct'] = 'v2' # Either v1 or v2 only

    # v2
    config['dirpath'] = {'videos_raw': 'VIDEOS',
                         'videos_annotated': 'videos_annotated',
                         'videos_frames': 'LIVE/FRAMES',
                         'images': 'LIVE/IMAGES',
                         'plates': 'LIVE/PLATES'
                         }

    # Video Structure
    config['video_input'] = {'ptuazon_underpass': "rtsp://root:pass@10.0.0.211/axis-media/media.amp",
                             'estrada': "rtsp://admin:@192.168.1.193/h264Preview_01_main",
                             'demo': "number_coding/sample_vid/tuazon6.avi"}.get(config['camera'], 0)

    config['video_fileprefix'] = 'video'
    config['video_secondsperfile'] = 10
    config['video_fps'] = 30.0
    ##TODO
    config['video_buffersize'] = int(config['video_fps'] * config['video_secondsperfile'])
    config['video_timeout'] = 0.5
    # Actor Framework
    config["actor_log_cfg"] = {'version': 1,
                        'formatters': {
                            'normal': {
                                'format': '%(levelname)-8s %(message)s'}},
                        'handlers': {
                            'h': {'class': 'logging.FileHandler',
                                  'filename': 'Actors.log',
                                  'formatter': 'normal',
                                  'level': logging.INFO}},
                        'loggers': {
                            '': {'handlers': ['h'], 'level': logging.DEBUG}}
                             }

    config['yolo_opts'] = {"config": 'number_coding/vehicledetect/cfg',
                           'binary': 'number_coding/vehicledetect/bin',
                           'imgdir': 'number_coding/vehicledetect/sample_img/',
                           "model": "number_coding/vehicledetect/cfg/yolo.cfg",
                           "load": "number_coding/vehicledetect/bin/yolo.weights",
                           "threshold": 0.1,
                           "gpu": 0.4}

    return config


if __name__ == '__main__':
    config = run_config()
    with open('config.json', 'w') as conff:
        json.dump(config, conff)

import numpy as np
import cv2
import redis

frame = cv2.imread("preview.png")

cv2.imshow("orig", frame)
cv2.waitKey(0)
cv2.destroyAllWindows()

r = redis.StrictRedis()

img_str = cv2.imencode('.png', frame)[1].tostring()
r.set("img", img_str)
r.set("sampleVal",10)
#nparr = np.fromstring(r.get("img"), np.uint8)
#img = cv2.imdecode(nparr, -1)
#cv2.imshow("sample", img)
#cv2.waitKey(0)

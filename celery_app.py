from __future__ import absolute_import, unicode_literals
from celery import Celery

CELERY_TASK_LIST = ['number_coding.platedetect.tasks', 'number_coding.vehicledetect.tasks']

app = Celery('number_coding', broker='pyamqp://guest@localhost:5672//', include=CELERY_TASK_LIST)

if __name__ == '__main__':
    app.start()
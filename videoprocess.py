import logging
from thespian.actors import *
import queue
import cv2
from thespian.troupe import troupe
from collections import deque
import time

from number_coding.vehicledetect.tasks import detectvehicles

file = '/home/aicatch/front-end-rails/public/MASTER/VIDEOS/ptuazon_underpass/2018/01/11/14/video20180111_140011.avi'

class VideoProcess(Actor):
    def __init__(self):
        super().__init__()
        self.framesdeq = deque()
        self.boolFirstFrame = True
        self.writer = cv2.VideoWriter()

    def receiveMessage(self, message, sender):
        self.troupe_work_in_progress = True
        logging.info('VideoProcess actor got: %s', message)
        if message['sender_name'] == 'frame_reader':
            if message['sender_cmd'] == 'write':
                self.send(sender, 'Acknowledged writing frame')
                self.initialize_process(message)
                self.add_to_queue(message)

            elif message['sender_cmd'] == 'process':
                self.finalize_process(message['name'], message['config'])
                self.send(sender, 'finalized_plates')

        elif message['sender_name'] == 'frame_writer':
                self.write_from_queue()

        else:
            self.send(sender, 'Command not found.')

        self.troupe_work_in_progress = False
        return

    def finalize_process(self, filename, opts):
        print("Finalizing processess... ")
        self.writer.release()
        self.boolFirstFrame = True
        detectvehicles.delay(filename, opts)

        return

    def write_from_queue(self):
        try:
            # Write Frame to the current video_name
            if self.writer.isOpened():
                while len(self.framesdeq) > 0:
                    imNew = self.framesdeq.pop()
                    self.writer.write(imNew)
                    framename = '%s_%.2f.png' % (self.frameprefix, self.iterframes/30)
                    cv2.imwrite(framename,framename)
                    self.iterframes += 1
                    #print("Successful in writing frame")

                #print("queue is now empty")
        except IndexError:
            print("No frames to write")
            pass


    def add_to_queue(self, message):
        # Add frame to deque
        #print ("Adding current frame to queue ")
        self.framesdeq.appendleft(message['current_frame'])
        message['sender_name'] = 'frame_writer'

        # Write frame
        self.send(self.myAddress, message)

    def initialize_process(self, config):
        curframe = config['current_frame']
        # Get writer settings
        if self.boolFirstFrame:
            self.iterframes = 0
            self.frameprefix = os.path.join(config['root'], config['dirpath']['videos_frame'],config['name'].rsplit('.', 1)[0])
            self.writer = cv2.VideoWriter(config['name'],
                                          cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
                                          30.0,
                                          (curframe.shape[1], curframe.shape[0]),
                                          True)
            print("Sucessful in creating writer")
            self.boolFirstFrame = False

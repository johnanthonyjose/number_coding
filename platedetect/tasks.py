import docker
from number_coding.celery_app import app
import requests

@app.task()
def execute_docker_static(name):
    client = docker.from_env()
    textdetdocker = client.containers.get('textdet')

    [print(i.decode()) for i in textdetdocker.exec_run(name, stream=True, tty=True, user='aicatch')]
    return

URL= "http://localhost:8080"
link = "/plates/"

@app.task()
def post_CTPN(payload):
    requests.post(URL+link, json=payload)
    return "successfully finished processing"
import logging
import docker
import os
from number_coding.jsonconfencoder import run_config
import requests
from pymongo import MongoClient
class GetPlateArea:

    def __init__(self, opts):
        super().__init__()
        self.dockerlogfile = 'dockerout.log'
        self.ctpnDir = '/home/aicatch/Documents/GitHub/CTPNv3'
        self.targetfileDir = ''
        print("Welcome to GetPlateArea actor!")
        self.initializer(opts)
        self.URL= "http://localhost:5051"
        self.link = "/getplates/"
        # self.client = MongoClient('mongodb://localhost:27017')
        # self.collection = self.client.live

    def initializer(self, config):
        # Get proper config
        self.localroot = config['root']
        self.dockerroot = config['dockerroot']
        self.dictdirpaths = config['dirpath']
        self.client = docker.from_env()

    # Method for getting the directory inside the docker container
    def getdockerdir(self, *args):
        pathdir = self.dockerroot
        try:
            for a in args:
                pathdir = os.path.join(pathdir, self.dictdirpaths[a])

        except KeyError:
            raise KeyError("You have the wrong key in your dictionary path")
        return pathdir

    def local2dockerdir(self, name):
        return name.replace(self.localroot, self.dockerroot)


    def start_dockerCTPN(self, name, mongoconn):
        try:
            from .tasks import execute_docker_static, post_CTPN

            print("Trying to enter docker")
            # filename = self.getdockerdir('videos_raw', 'file')
            filename = self.local2dockerdir(name)
            mongoconn.vehicles.find_one_and_update({'framesequence_name': name}, {'$set': {'framesequence_name': filename}})


            # arg= 'VIDEOS/estrada/2018/01/05/17/video20180105_171732.avi'
            defacto = 'python tools/process_vid3.py -i %s --image-dir %s --plate-dir %s' \
                      % (filename,
                         self.getdockerdir('images'),
                         self.getdockerdir('plates'))

            payload = { 'vidseq': filename,
                        'imgpath': self.getdockerdir('images'),
                        'platepath': self.getdockerdir('plates')}

            print(defacto)
            job = execute_docker_static.delay(defacto)
            #job = post_CTPN.delay(payload)
            #job = q.enqueue_call(func=execute_docker_static, args=(defacto,), timeout=600)

        except requests.exceptions.HTTPError as e:
            logstring = e
            print("No running containers...", e.args[0])
            with open(self.dockerlogfile, 'ab') as f:
                f.write(logstring)
                [f.write(container.logs()) for container in self.client.containers.list() if container]

        except TypeError as e:
            print('Am I exiting myself?', e)

        except docker.errors.ContainerError as e:
            logstring = e
            print('container error ', e)
        except KeyError as e:
            raise KeyError("You have the wrong directory")
        except Exception as e:
            logstring = e
            raise Exception(e)

        return 0

def test_getplatearea2(file, conn):
    # Get configuration file
    import os
    print ("my working directory", os.getcwd())
    conf = run_config()
    print ("initialize plater object")
    plater = GetPlateArea(conf)
    print ("start detecting plates")
    plater.start_dockerCTPN(file, conn)
    return


if __name__=='__main__':
    test_getplatearea2()
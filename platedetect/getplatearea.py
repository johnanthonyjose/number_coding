import logging
import docker
import requests
import os


class GetPlateArea:

    def __init__(self):
        self.dockerlogfile = 'dockerout.log'
        self.ctpnDir = '/home/aicatch/Documents/GitHub/CTPNv3'
        self.dockerroot = 'dockerdef'
        self.localroot = ''
        self.dictdirpaths = dict()
        self.qtargetfileDir = ''
        # Tell RQ what Redis connection to use

    # Method for getting the directory inside the docker container
    def getdockerdir(self, *args):
        pathdir = self.dockerroot
        try:
            for a in args:
                pathdir = os.path.join(pathdir, self.dictdirpaths[a])
        except KeyError:
            raise KeyError("You have the wrong key in your dictionary path")
        return pathdir

    # Method for getting the directory in the local folder
    def getlocaldir(self, *args):
        if 'videos_raw' in args:
            return self.targetfileDir
        else:
            pathdir = self.localroot
            # Recursive path addition
            for a in args:
                pathdir = os.path.join(pathdir, self.dictdirpaths[a])

        if not os.path.exists(pathdir):
            os.makedirs(pathdir)
            print("Directory %s does not exists. creating..." % pathdir)

        return pathdir

    def getvolumesdir(self):
        return {self.getlocaldir(key): {'bind': self.getdockerdir(key),
                                        'mode': 'rw'}
                for key, value in self.dictdirpaths.items()
                if key not in ['file', 'videos_annotated']}

    def local2dockerdir(self, name, config):
        return name.replace(config['root'], config['dockerroot'])

    def getplates(self, message):
        if message:
            print("Welcome to GetPlateArea actor!")
            try:
                # Get proper config
                self.localroot = message['config']['root']
                self.dockerroot = message['config']['dockerroot']
                self.dictdirpaths = message['config']['dirpath']

                # Name contains the full path.
                # self.targetfileDir, rawname = message['name'].rsplit('/', 1)
                # self.dictdirpaths['file'] = rawname

                print("Processing the video %s: " % (message['name']))
                # filename = self.getdockerdir('videos_raw', 'file')
                self.filename = self.local2dockerdir(message['name'], message['config'])
                print("In docker it's ", self.filename)

                # Start dockerization!
                self.initialize_dockerCTPN()
            except TypeError as e:
                print('Am I exiting myself?', e)
            finally:
                return ("It's done dear.")

    def initialize_dockerCTPN(self):
        try:
            print("Trying to enter docker")

            defacto = 'python tools/process_vid2.py -i getplatearea.py%s --image-dir %s --plate-dir %s' \
                      % (self.filename,
                         self.getdockerdir('images'),
                         self.getdockerdir('plates'))

            job = execute_docker_static.delay(defacto)

        except requests.exceptions.HTTPError as e:
            logstring = e
            print("No running containers...", e.args[0])
            with open(self.dockerlogfile, 'ab') as f:
                f.write(logstring)
                [f.write(container.logs()) for container in self.client.containers.list() if container]
        except docker.errors.ContainerError as e:
            logstring = e
            print('container error ', e)
            pass
        except KeyError as e:
            raise KeyError("You have the wrong directory")
        except Exception as e:
            logstring = e
            print('error ', e)

        return 0

    def restart_docker(self):
        pass

    def run_docker_ctpn(self, filename):
        print("My docker image folder", {**{'/tmp/.X11-unix': {'bind': '/tmp/.X11-unix',
                                                               'mode': 'ro'},
                                            os.path.join(self.ctpnDir, 'tools'): {'bind': '/opt/CTPN/tools',
                                                                                  'mode': 'ro'}},
                                         **self.getvolumesdir()})
        return self.client.containers.run(
            "ctpnv3:latest",
            command=[os.path.join('tools', 'process_vid.py'),
                     '-i', filename,
                     '--image-dir', self.getdockerdir(self.dictdirpaths['images']),
                     '--plate-dir', self.getdockerdir(self.dictdirpaths['plates'])],
            volumes={**{'/tmp/.X11-unix': {'bind': '/tmp/.X11-unix',
                                           'mode': 'ro'},
                        os.path.join(self.ctpnDir, 'tools'): {'bind': '/opt/CTPN/tools',
                                                              'mode': 'ro'}},
                     **self.getvolumesdir()},
            runtime='nvidia',
            environment=dict(DISPLAY=os.environ['DISPLAY']),
            detach=True,
            tty=True,
            user=1000,
            log_config=dict(type='syslog', config={'syslog-address': 'udp://127.0.0.1:5514',
                                                   'syslog-facility': 'daemon',
                                                   'tag': 'GetPlateActor_' + str(self.myAddress)})
        )

if __name__=='__main__':
